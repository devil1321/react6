describe('interface test',()=>{
    // before(()=>{
    //     uruchomienie tylko raz na samym poczatku
    // })
      beforeEach(()=>{
          cy.log('rozpoczynam testowanie')
      })
    //   afterEach(()=>{})
    //   afterEach(()=>{})
    let fill = ()=>{}
    it('add task',()=>{
        let name = 'Task ' 
        let taskNum = 1 
        let years = 2000
        cy.visit('http://localhost:3000/')
        fill = () =>{
            for(let i = 0; i<6; i++){
                let date = `${years}-01-01`
                cy.get('input[name="name"]').type(name + taskNum)
                cy.get('input[name="date"]').type(date)
                cy.get('button[type="submit"').click()
                taskNum++
                years++
            }       
        }
    })
    it('change task' ,()=>{
        fill()
        let name = 'Changed Task '
        let taskNum = 1 
        cy.get('input[name="list-name"]').each(($el, index, $list)=>{
            cy.wrap($el).clear()
            cy.wrap($el).type(name+taskNum)
            taskNum++
        })
    })
    it('change date' ,()=>{
        let years = 2000
        cy.get('input[name="list-date"]').each(($el, index, $list)=>{
                let date = `${years}-01-01`
                    cy.wrap($el).type(date)
                    years++
                })
    })
    it('change checkbox',()=>{
        cy.get('input[name="checkbox"]').each(($el, index, $list)=>{
            cy.wrap($el).check()
        })
        cy.get('input[name="checkbox"]').each(($el, index, $list)=>{
            cy.wrap($el).uncheck()
        })
    })
    it('check buttons and delete tasks',()=>{
        cy.get('button').each(($el, index, $list)=>{
            if($el.hasClass('add')){
                cy.wrap($el).contains('Add Task').click()
            }
            else if(!$el.hasClass('clear')){
                cy.wrap($el).contains('Delete').click()
            }
            else{
                cy.wrap($el).contains('Clear List').click()
            }
        })
    })
    it('fill and clear tasks',()=>{
        fill()
        cy.get('button').contains('Clear List').click()     
    })
})