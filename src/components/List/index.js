import React,{useContext} from 'react'
import {Logic} from '../../context'
import "./list.css"
const List = (props) => {
    const {tasks,dispatch} = useContext(Logic)
    return (
        <div className="list-wrapper">
            <h2>Your Tasks</h2>
            <table className="list">
               
                    <thead className="list__head">
                        <tr>
                            <th>Name</th>
                            <th>Time</th>
                            <th>Finished</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                <tbody>
                {tasks.map(task=>{
                    return  <tr key={task.id} className="list__row">
                                <td className="list__item"><input name="list-name" onChange={(e)=>{dispatch({e:e,id:task.id,type:'handleName'})}} value={task.name} required/></td>
                                <td className="list__item"><input name="list-date" onChange={(e)=>{dispatch({e:e,id:task.id,type:'handleDate'})}} type="date" value={task.date} required/></td>
                                <td className="list__item">
                                  {task.comleted ? 
                                    (<input name="checkbox" onChange={(e)=>{dispatch({e:e,id:task.id,completed:task.completed,type:'handleCompleted'})}} type="checkbox" className="list__checkbox" checked/>) 
                                    : 
                                    (<input name="checkbox" onChange={(e)=>{dispatch({e:e,id:task.id,completed:task.completed,type:'handleCompleted'})}} type="checkbox" className="list__checkbox"/>)}
                                </td>
                                <td className="list__item"><button  onClick={(e)=>{dispatch({e:e,id:task.id,type:'handleDelete'})}} className="btn">Delete</button></td>                                
                            </tr>
                })}
                </tbody>
            </table>
            <button className="btn clear" onClick={()=>{dispatch({type:'handleClear'})}}>Clear List</button>
        </div>
    )
}

export default List