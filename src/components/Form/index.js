import React ,{useState,useContext} from 'react'
import { v4 as uuidv4 } from 'uuid';
import {Logic} from '../../context'
import './form.css'
const Form = (props) => {
    const {tasks,setTasks} = useContext(Logic)
    const [date,setDate] = useState('')
    const [name,setName] = useState('')
    const completed = false
    const handleName = (e) =>{
        setName(e.target.value)
    }
    const handleDate = (e) =>{
        setDate(e.target.value)
    }
    const handleSubmit = (e) =>{
        const tempTasks = tasks
        e.preventDefault()
        const id = uuidv4()
        const task = {
                id,
                name,
                date,
                completed
            }
        if(name !== "" && date !== ""){    
            tempTasks.push(task)
        }
        setDate('')
        setName('')
        setTasks(tempTasks)  
        props.setReload(!props.reload)
    }
    return (
        <div className="form-wrapper" onSubmit={(e)=>handleSubmit(e)}>
            <h2>Add Task</h2>
            <form action="" className="form">   
                <label htmlFor="">Task Name</label>
                <input type="text" className="form__control" name="name" value={name} onChange={(e)=>{handleName(e)}}/>
                <label htmlFor="">Enter Finish Date</label>
                <input type="date" className="form__control" name="date" value={date} onChange={(e)=>{handleDate(e)}} />
                <button type="submit" className="btn add">Add Task</button>
            </form>
        </div>
    )
}
export default Form