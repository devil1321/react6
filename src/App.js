import {useState} from 'react'
import './App.css';
import Form from './components/Form'
import List from './components/List'
import {LogicStatsProvider} from './context'
function App() {

  const [reload,setReload] = useState(false)

  return (
    <div className="App">
      <LogicStatsProvider>
        <List />
        <Form reload = {reload} setReload = {setReload}/>
      </LogicStatsProvider>
    </div>
  );
}

export default App;
