import React,{createContext,useState,useReducer}  from 'react'
export const Logic = createContext({
   tasks:[],
   setTasks:item =>{},
   state:null,
   dispatch:item=>{}
})
export const LogicStatsProvider = ({children}) =>{
    const [tasks,setTasks] = useState([{id:"id",name:"Your Task Name",date:"2014-06-12",completed:false}])
    const initialState = {
        task:{name:'',date:'',completed:false}
    }

    const reducer = (state, action) =>{
        switch(action.type){
            case 'handleName':
                let tempTasks = tasks
                let id1 = action.id
                state.task.name = action.e.target.value
                let tempTask1 = tasks.find(task => task.id === id1)
                let index1 = tasks.indexOf(tempTask1)
                tempTasks[index1].name = state.task.name
                setTasks(tempTasks)
                return {task:{name:state.task.name}}
            case 'handleDate':
                let tempTasks2 = tasks
                let id2 = action.id
                state.task.date = action.e.target.value
                let tempTask2 = tasks.find(task => task.id === id2)
                let index2 = tasks.indexOf(tempTask2)
                tempTasks2[index2].date = state.task.date
                setTasks(tempTasks2)
                return {task:{date:action.e.target.value}}
            case 'handleCompleted':
                let id3 = action.id
                let tempTasks3 = tasks
                let tempTask3 = tasks.find(task => task.id === id3)
                let index3 = tasks.indexOf(tempTask3)
                if(tempTasks3[index3] !== undefined){
                    if(tempTasks3[index3].completed){
                        tempTasks3[index3].completed = false
                    } else{
                        tempTasks3[index3].completed = true
                    }
                    setTasks(tempTasks3)
                }
                break
            case 'handleDelete':
                let id4 = action.id
                setTasks(tasks.filter(task => task.id !== id4))
                break
            case 'handleClear':
                setTasks([])
                break
            default:
               throw new Error()
        }
    }
    const [state,dispatch] = useReducer(reducer,initialState)

 
    return(
        <Logic.Provider value={{tasks,setTasks,state,dispatch}}>
            {children}
        </Logic.Provider>
    )
}
